#!/usr/bin./env python

from setuptools import find_packages, setup


setup(
    name='resume',
    version='0.0',
    description='Resume hosting site',
    license='LICENSE',
    packages=find_packages(),
    install_requires=[
        'flask',
        'flask-restful',
        'flask-sqlalchemy',
    ],
)