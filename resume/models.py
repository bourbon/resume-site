import datetime

from flask_sqlalchemy import SQLAlchemy

from resume.utils import generate_password_hash


db = SQLAlchemy()


class User(db.Model):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    role = db.Column(db.Integer, nullable=False, default=1)
    name = db.Column(db.String(255), nullable=False)
    username = db.Column(db.String(255), nullable=False, unique=True)
    password = db.Column(db.String(255))

    resumes = db.relationship('ResumeModel', backref='user')

    def __repr__(self):
        return '<User %r>' % self.username

    @property
    def is_admin(self):
        return self.role == 0

    @property
    def password(self):
        raise AttributeError("Write-only attribute")

    @property
    def serialize(self):
        return {
            'id': self.id,
            'role': self.role,
            'name': self.name,
            'username': self.username,
            'resumes': self.resumes
        }

    @password.setter
    def password(self, value):
        self.password = generate_password_hash(value)


class ResumeModel(db.Model):
    __tablename__ = 'resume'

    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(db.String(255), unique=True, nullable=False)
    created = db.Column(db.DateTime, nullable=False, default=datetime.datetime.now)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    sections = db.relationship('SectionModel', backref='resume')

    def __repr__(self):
        return '<Resume %r>' % self.id

    @property
    def serialize(self):
        return {
            'id': self.id,
            'uuid': self.uuid,
            'created': self.created,
            'user_id': self.user_id,
            'sections': self.sections
        }


class SectionModel(db.Model):
    __tablename__ = 'section'

    id = db.Column(db.Integer, primary_key=True)
    header = db.Column(db.String(255), nullable=False)
    start_date = db.Column(db.DateTime, nullable=False)
    end_date = db.Column(db.DateTime, nullable=True)
    resume_id = db.Column(db.Integer, db.ForeignKey('resume.id'))

    contents = db.relationship('ContentModel', backref='section')

    def __repr__(self):
        return '<Section %r>' % self.id

    @property
    def serialize(self):
        return {
            'id': self.id,
            'header': self.header,
            'start_date': self.start_date,
            'end_date': self.end_date,
            'resume_id': self.resume_id,
            'contents': self.contents
        }


class ContentModel(db.Model):
    __tablename__ = 'content'

    id = db.Column(db.Integer, primary_key=True)
    role = db.Column(db.String(255), nullable=False)
    body = db.Column(db.String(2048), nullable=False)
    section_id = db.Column(db.Integer, db.ForeignKey('section.id'))

    def __repr__(self):
        return '<Content %r>' % self.id

    @property
    def serialize(self):
        return {
            'id': self.id,
            'role': self.role,
            'body': self.body,
            'section_id': self.section_id
        }
