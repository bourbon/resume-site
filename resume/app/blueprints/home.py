from flask import flash, Blueprint, redirect, request, session, url_for

from resume.decorators import templated
from resume.models import User, db

app = Blueprint('home', __name__)


@app.route('/')
@templated()
def index():
    return 'Index'


@app.route('/login', methods=['GET', 'POST'])
def login():
    if session.get('user_id'):
        redirect(url_for('home'))
    if request.method == 'POST':
        un = request.form.get('username')
        pw = request.form.get('password')
        user = User.query.filterby(username=un, password=pw).first()
        if user is None:
            flash('Username or Password is invalid', 'error')
            return redirect(url_for('login'))
    return redirect(request.args.get('next') or url_for('index'))
