from flask import jsonify
from flask_restful import Resource, abort, reqparse

from resume.models import SectionModel, ResumeModel, db


parser = reqparse.RequestParser()
parser.add_argument('header')
parser.add_argument('start_date')
parser.add_argument('end_date')


def abort_if_no_section(s_id):
    s = SectionModel.query.filter_by(id=s_id)
    if not s:
        abort(404, message="Section object (ID: {}) doesn't exist!".format(s_id))


class Section(Resource):
    def get(self, s_id):
        abort_if_no_section(s_id)
        s = SectionModel.query.filter_by(id=s_id)
        return jsonify(s.all())

    def delete(self, s_id):
        abort_if_no_section(s_id)
        s = SectionModel.query.filter_by(id=s_id)
        db.session.delete(s)
        db.session.commit()
        return 'success', 204

    def put(self, s_id):
        abort_if_no_section(s_id)
        s = SectionModel.query.filter_by(id=s_id)
        args = parser.parse_args()

        if args.header:
            s.header = args.header
        if args.start_date:
            s.start_date = args.start_date
        if args.end_date:
            s.end_date = args.end_date

        db.session.commit()
        return '', 201


class SectionList(Resource):
    def get(self, uuid):
        out = []
        for c in ResumeModel.query.filter_by(uuid=uuid).first().contents:
            out.append(c.to_dict())
        return jsonify(out)

    def post(self, uuid):
        r = ResumeModel(uuid=uuid)
        args = parser.parse_args()
        s = SectionModel(args.header, args.start_date, args.end_date)
        r.sections.append(s)
        s_id = db.session.commit()
        return jsonify({'id': s_id}), 200
