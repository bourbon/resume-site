import datetime

from flask import Blueprint, jsonify
from flask_restful import Resource, Api, abort, reqparse

from resume.models import ResumeModel, db
from resume.utils import get_uuid

from .content import Content, ContentList
from .section import Section, SectionList


app = Blueprint('resume', __name__)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('author')


def abort_if_no_resume(uuid):
    r = ResumeModel.query.filter_by(uuid=uuid)
    if not r:
        abort(404, message="Resume object (uuid: {}) doesn't exist!".format(uuid))


class Resume(Resource):
    def get(self, uuid):
        abort_if_no_resume(uuid)
        r = ResumeModel.query.filter_by(uuid=uuid)
        return jsonify(r.all())

    def delete(self, uuid):
        abort_if_no_resume(uuid)
        r = ResumeModel.query.filter_by(uuid=uuid)
        db.session.delete(r)
        db.session.commit()
        return 'success', 204

    def put(self, uuid):
        abort_if_no_resume(uuid)
        r = ResumeModel.query.filter_by(uuid=uuid)
        args = parser.parse_args()

        if args.author:
            r.author = args.author

        db.session.commit()
        return '', 201


class ResumeList(Resource):
    def get(self):
        out = []
        for r in ResumeModel.query.all():
            out.append(r.uuid)
        return jsonify(out)

    def post(self):
        args = parser.parse_args()
        dt = datetime.datetime.now()
        uuid = get_uuid()
        r = ResumeModel(uuid, dt, args.author)
        db.session.add(r)
        r_id = db.session.commit()
        return jsonify({'id': r_id, 'uuid': uuid})


api.add_resource(Resume, '/resume/<uuid>')
api.add_resource(ResumeList, '/resume')
api.add_resource(Section, '/resume/<uuid>/<section>')
api.add_resource(SectionList, '/resume/<uuid>/sections')
api.add_resource(Content, '/resume/<uuid>/<section>/<content>')
api.add_resource(ContentList, '/resume/<uuid>/<section>/contents')
