from flask import jsonify
from flask_restful import Resource, abort, reqparse

from resume.models import ContentModel, SectionModel, db


parser = reqparse.RequestParser()
parser.add_argument('role')
parser.add_argument('body')


def abort_if_no_content(c_id):
    c = ContentModel.query.filter_by(id=c_id)
    if not c:
        abort(404, message="Content object (ID: {}) doesn't exist!".format(c_id))


class Content(Resource):
    def get(self, content):
        abort_if_no_content(content)
        c = ContentModel.query.filter_by(id=content)
        return jsonify(c.all())

    def delete(self, content):
        abort_if_no_content(content)
        c = ContentModel.query.filter_by(id=content)
        db.session.delete(c)
        db.session.commit()
        return 'success', 204

    def put(self, content):
        abort_if_no_content(content)
        c = ContentModel.query.filter_by(id=content)
        args = parser.parse_args()

        if args.body:
            c.body = args.body
        if args.role:
            c.role = args.role

        db.session.commit()
        return '', 201


class ContentList(Resource):
    def get(self, section):
        out = []
        for c in SectionModel.query.filter_by(id=section).first().contents:
            out.append(c.to_dict())
        return jsonify(out)

    def post(self, section):
        s = SectionModel(id=section)
        args = parser.parse_args()
        c = ContentModel(args.role, args.body)
        s.contents.append(c)
        c_id = db.session.commit()
        return jsonify({'id': c_id}), 200
