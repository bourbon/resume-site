import errno
import os

from flask import Flask

from resume.app.blueprints.home import app as home_app
from resume.app.blueprints.resume import app as resume_app
from resume.config import DevelopmentConfig, ProductionConfig, TestingConfig
from resume.models import db


def create_app():
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(DevelopmentConfig)

    try:
        os.makedirs(app.instance_path)
    except OSError as e:
        if e.errno != errno.EEXIST:  # ignore error that the file exists
            raise

    app.register_blueprint(home_app)
    app.register_blueprint(resume_app)
    db.init_app(app)

    @app.errorhandler(404)
    def page_not_found(e):
        return e

    @app.errorhandler(500)
    def server_error(e):
        return e

    return app


if __name__ == '__main__':
    app = create_app()
    app.app_context().push()
    db.create_all()
    app.run()
